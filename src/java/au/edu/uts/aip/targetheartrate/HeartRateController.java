/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.edu.uts.aip.targetheartrate;

import java.io.*;
import javax.enterprise.context.*;
import javax.inject.*;
import javax.validation.constraints.*;

/**
 *
 * @author 01062443
 */
@Named
@SessionScoped
public class HeartRateController implements Serializable {

    private double heartRate;
    private int age;
    private int intensity = 100;

    public void setAge(int age) {
        this.age = age;
    }

    @Min(0)
    @Max(160)    
    public int getAge() {
        return this.age;
    }

    public double getHeartRate() {
        double percentage = (double)this.intensity / 100;
        this.heartRate = ((160 - this.age) * percentage) + 60;
        return this.heartRate;
    }
    
    @Min(0)
    @Max(100)
    public int getIntensity() {
        return this.intensity;
    }
    
    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }
}
